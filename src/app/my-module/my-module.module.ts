import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetColorDirective } from 'src/app/my-module/set-color.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SetColorDirective],
  exports: [
    SetColorDirective
  ]
})
export class MyModuleModule { }
