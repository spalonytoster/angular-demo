import { Directive, Input, SimpleChanges, OnChanges, Renderer2 } from '@angular/core';
import { ElementRef } from '@angular/core';

@Directive({
  selector: '[appSetColor]'
})
export class SetColorDirective {

@Input() set appSetColor(value) {
  this.renderer.setStyle(
    this.el.nativeElement,
    'background',
    value);
}

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {

  }
}
