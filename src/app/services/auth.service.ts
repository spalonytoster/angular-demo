import { Injectable } from '@angular/core';
import { AuthServiceInterface, AuthDataInterface, HttpResponseInterface } from 'src/app/services/interfaces';
import { HttpClient } from '@angular/common/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Settings } from 'src/app/utils/settings';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements AuthServiceInterface, CanActivate {

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean  {
    return this._access;
  }
  token: String = "";
  private _access: any = false;

  constructor(private http: HttpClient) {
    this.logged();
  }

  logIn(authData: AuthDataInterface): void {
    this.http
      .post(Settings.LOGIN_END_POINT, authData)
      .subscribe((res: HttpResponseInterface) => {
        console.log(res.ok + ": " + res.msg)
        // this.token = res.token;
        this._access = res.ok;
      });
  }

	logged(): void {
    this.http
      .get(Settings.LOGGED_END_POINT)
      .subscribe((res: HttpResponseInterface) => {
        console.log(res.ok + ": " + res.msg);
        this._access = res.ok;
      });
	}

	logOut(): void {
    this.http
      .get(Settings.LOGOUT_END_POINT)
      .subscribe((res: HttpResponseInterface) => {
        console.log(res.ok + ": " + res.msg)
        this._access = false;
      });
	}

  get access() {
    return this._access;
  }
}
