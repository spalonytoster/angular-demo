import { Injectable } from '@angular/core';
import { HttpServiceInterface } from 'src/app/services/interfaces';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Settings } from 'src/app/utils/settings';

@Injectable({
  providedIn: 'root'
})
export class WorkersService implements HttpServiceInterface {

  workers: any[] = []

  constructor(private http: HttpClient) { }

  fetch(params?: any): Observable<any> {
    return this.http
      .get(Settings.WORKERS_END_POINT, params);
  }

  add(item: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  update(item: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  remove(id: any): Observable<any> {
    return this.http
      .delete(`${Settings.WORKERS_END_POINT}/${id}`)
  }
}
