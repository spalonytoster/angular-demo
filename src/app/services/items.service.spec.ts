import { TestBed, inject } from '@angular/core/testing';

import { ItemsService } from './items.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Settings } from 'src/app/utils/settings';

fdescribe('ItemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ItemsService]
    });
  });

  it('should be created', inject([ItemsService], (service: ItemsService) => {
    expect(service).toBeTruthy();
  }));

  it('should return any object when fetch called', inject([ItemsService], (service: ItemsService) => {
    // given
    const itemsService = TestBed.get(ItemsService);
    const http = TestBed.get(HttpTestingController);
    let expected = [1, 2, 3];

    // when
    itemsService.fetch()
      .subscribe((res) => {
        console.log(res);
        expected = res;
      });

    // then
    const req = http.expectOne(Settings.ITEMS_END_POINT);
    expect(req.request.method).toEqual('GET');
    req.flush([1, 2, 3]);
    expect(expected).toEqual([1, 2, 3]);
    http.verify();
  }));
});
