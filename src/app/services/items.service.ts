import { Injectable } from '@angular/core';
import { HttpServiceInterface } from 'src/app/services/interfaces';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Settings } from 'src/app/utils/settings';

@Injectable({
  providedIn: 'root'
})
export class ItemsService implements HttpServiceInterface {

  items: any[] = []

  constructor(private http: HttpClient) { }

  fetch(params?: any): Observable<any> {
    return this.http
      .get(Settings.ITEMS_END_POINT, { params });
  }

  get(id?: any): Observable<any> {
    return this.http
      .get(`${Settings.ITEMS_END_POINT}/${id}`);
  }

  add(item: any): Observable<any> {
    return this.http
      .post(Settings.ITEMS_END_POINT, item);
  }

  update(item: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  remove(id: any): Observable<any> {
    return this.http
      .delete(`${Settings.ITEMS_END_POINT}/${id}`)
  }
}
