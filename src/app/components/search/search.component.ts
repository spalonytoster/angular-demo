import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { filter } from 'rxjs/internal/operators/filter';

@Component({
  selector: 'ca-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, AfterViewInit {

  @Input() controls: any[];
  @ViewChild('f') form: NgForm;
  @Input() filters: BehaviorSubject<any>;

  constructor() { }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    console.log('after view init');
    console.log('form:', this.form);
    this.form.valueChanges
      .pipe(
        debounceTime(500)
        // ,filter((value: any) => {
        //   debugger;
        //   if (value && value.includes('dupa')) {
        //     alert('sam jestes ' + value.title);
        //     return false;
        //   } else {
        //     return true;
        //   }
        // })
      )
      .subscribe((changes) => {
        this.filters.next({
          ...this.filters.value,
          ...changes
        });
        console.log(this.filters.value);
      });
  }
}
