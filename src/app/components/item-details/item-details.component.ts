import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ItemInterface } from 'src/app/services/interfaces';
import { ItemsService } from 'src/app/services/items.service';

@Component({
  selector: 'ca-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {

  item: ItemInterface;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ItemsService
  ) {}

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service
          .get(params.get('id'))
      )
    )
    .subscribe((res) => {
      console.log(res);
      this.item = res.data;
    });
  }

}
