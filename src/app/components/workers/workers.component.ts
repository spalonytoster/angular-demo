import { Component, OnInit } from '@angular/core';
import { WorkersService } from '../../services/workers.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'ca-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.css']
})
export class WorkersComponent implements OnInit {

  workers: any[] = []

  config: any[] = [
    { key: 'name', label: 'Name' },
    { key: 'phone', label: 'Phone' },
    { key: 'category', label: 'Category'}
  ];

  filters: BehaviorSubject<any> = new BehaviorSubject({
    itemsPerPage: '5',
    currentPage: '1'
  });

  searchControls: any[] = ['name', 'phone', 'category']

  constructor(private workersService: WorkersService) {
    this.fetch();

    this.filters.subscribe((value) => {
      console.log('filters:', value);
    });
  }

  ngOnInit() {
  }

  private fetch() {
    this.workersService
      .fetch(this.filters.value)
      .subscribe((res) => {
        this.workers = res.data
      });
  }

  removeWorker(id) {
    console.log(`Removing worker with id: ${id}`);
    this.fetch();
  }

}
