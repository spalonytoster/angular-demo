import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../../services/items.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { ItemInterface } from 'src/app/services/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'ca-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  config: any[] = [
    { key: 'title', label: 'Title' },
    { key: 'price', type: 'input', label: 'Price' },
    { key: 'imgSrc', type: 'image', label: 'Image'}
  ];

  items = [];
  total = 0;

  filters: BehaviorSubject<any> = new BehaviorSubject({
    itemsPerPage: '5',
    currentPage: '1'
  });

  newItem: Subject<ItemInterface> = new Subject();

  searchControls: any[] = ['title', 'priceFrom']

  constructor(private itemsService: ItemsService,
              private router: Router) {
    this.filters.subscribe((value) => {
      console.log('filters:', value);
      this.fetch();
    });

    this.newItem
      .subscribe((item) => {
        console.log('new item: ', item);
        this.itemsService
          .add(item)
          .subscribe((res) => {
            console.log(res.msg, ' item with id:', res.id)
            this.fetch();
          });
      });
  }

  ngOnInit() {
    // this.newItem
    //   .subscribe((value) => {
    //     if (value.type === 'added') {
    //       // TODO: close modal
    //     }
    //   });
  }

  private fetch() {
    this.itemsService
      .fetch(this.filters.value)
      .subscribe((res) => {
        console.log(res.total)
        this.items = res.data
        this.total = res.total;
      });
  }

  removeItem(itemId) {
    console.log(`Removing item with id: ${itemId}`);
    this.itemsService
      .remove(itemId)
      .subscribe((res) => {
        console.log(res)
        this.fetch();
      });
  }

  showItem(itemId) {
    this.router.navigate(['item-details', itemId]);
  }

  updateFilters(key, value) {
    this.filters.next({
      ...this.filters.value,
      [key]: value
    });
  }
}
