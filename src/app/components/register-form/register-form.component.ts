import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Settings } from 'src/app/utils/settings';
import { map, debounceTime } from 'rxjs/operators';
import { HttpResponseInterface } from 'src/app/services/interfaces';
import { CustomValidators } from 'src/app/utils/custom-validators';

@Component({
  selector: 'ca-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  form: FormGroup;

  constructor(private http: HttpClient) {
    this.form = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.email
      ], this.checkUser.bind(this)),
      // ], () => { this.checkUser() }),
      birthDate: new FormControl('', [
        Validators.required,
        CustomValidators.passedDate
      ]),
      hobbies: new FormGroup({
        gaming: new FormControl(),
        reading: new FormControl(),
        music: new FormControl(),
        photography: new FormControl()
      }, [
        CustomValidators.atLeastOne
      ])
    });

    this.form.valueChanges
      .pipe(
        debounceTime(500),
        map((value) => {
          return { ...value, modifiedAt: Date.now() }
        })
      )
      .subscribe((value) => {
        console.log('form change');
        console.log(value);
      });
  }

  ngOnInit() {
  }

  checkUser(control: AbstractControl): Observable<any> {
    return this.http
      .get(
        Settings.DOES_IT_EXIST,
        {
          params: { username: control.value }
        }
      )
      .pipe(
        map((res: HttpResponseInterface) => {
          if (res.ok) {
              return null;
          } else {
            return { err: res.error }
          }
        })
      );
  }
}
