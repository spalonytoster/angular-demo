import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ca-datagrid',
  templateUrl: './datagrid.component.html',
  styleUrls: ['./datagrid.component.css']
})
export class DatagridComponent implements OnInit {

  @Input() data: any[] = []
  @Input() config: any[] = []
  @Output() removeEvent = new EventEmitter();
  @Output() showEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  remove(rowId) {
    console.log(rowId)
    this.removeEvent.emit(rowId);
  }

  show(rowId) {
    this.showEvent.emit(rowId);
  }
}
