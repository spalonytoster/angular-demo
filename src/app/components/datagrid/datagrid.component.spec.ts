import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { DatagridComponent } from './datagrid.component';
import { By } from '@angular/platform-browser';

describe('DatagridComponent', () => {
  let component: DatagridComponent;
  let fixture: ComponentFixture<DatagridComponent>;
  let template: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatagridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatagridComponent);
    component = fixture.componentInstance;
    template = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display 2 rows', () => {
    const rows = template.queryAll(By.css('tbody tr'));
    expect(rows.length).toBe(0);
    component.data = [{ title: 'tomato'}, { title: 'onion' }];
    fixture.detectChanges();
    const rows2 = template.queryAll(By.css('tbody tr'));
    expect(rows2.length).toBe(2);
  });

  it('should display 2 rows with tomato', () => {
    component.data = [{ title: 'tomato'}, { title: 'onion' }];
    component.config = [{ key: 'title'}];
    fixture.detectChanges();
    const rows = template.queryAll(By.css('tbody tr'));
    // console.log(rows[0])
  });
});
