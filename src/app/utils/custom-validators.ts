import { AbstractControl, ValidationErrors } from "@angular/forms";

export class CustomValidators {
  static atLeastOne(control: AbstractControl): ValidationErrors {
    let hobbies = control.value;
    if (Object.values(hobbies).some(hobby => !!hobby)) {
      return null;
    }
    return { atLeastOne: false };
  }

  static passedDate(control: AbstractControl): ValidationErrors {
    let date = Date.parse(control.value);
    let now = Date.now()
    if (date < now) {
        return null;
    }
    return {
      passedDate: true
    }
  }
}
