import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(objects: any[], filters: any): any {
    return objects.filter((obj) => {
      for (let key in filters) {
        if (obj.hasOwnProperty(key)) {
          let value = obj[key];
          if (typeof value === 'string' && value !== "") {
            value = value.toLowerCase();
            if (!value.includes(filters[key].toLowerCase())) {
              return false;
            }
          }
        }
      }
      return true;
    });
  }
}
