import { Pipe, PipeTransform } from '@angular/core';
import { DictService } from 'src/app/services/dict.service';

@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {

  DEFAULT_ERROR = 'jakis blad';

  constructor(private dictService: DictService) {}

  transform(key: any, args?: any): any {
    let lang = !!args ? args : 'de';

    if (this.dictService.languages.hasOwnProperty(lang)) {
      return this.dictService.languages[lang][key];
    }
    return this.DEFAULT_ERROR;
  }

}
