import { MapToIterablePipe } from './map-to-iterable.pipe';

describe('MapToIterablePipe', () => {
  it('create an instance', () => {
    const pipe = new MapToIterablePipe();
    expect(pipe).toBeTruthy();
  });

  it('Should return array', () => {
    const pipe = new MapToIterablePipe();
    const result = pipe.transform({ id: 1 });
    expect(result).toEqual(['id']);
  });
});
