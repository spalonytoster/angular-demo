import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InitComponent } from './init/init.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: 'lazy', component: InitComponent }
    ])
  ],
  declarations: [InitComponent],
  exports: [InitComponent]
})
export class MyLazyModule { }
