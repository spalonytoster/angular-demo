import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ItemsComponent } from './components/items/items.component';
import { WorkersComponent } from './components/workers/workers.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { SearchComponent } from './components/search/search.component';
import { DatagridComponent } from './components/datagrid/datagrid.component';
import { AuthComponent } from './components/auth/auth.component';
import { CORSInterceptor } from 'src/app/utils/cors-interceptor';
import { AddItemComponent } from './add-item/add-item.component';
import { SearchPipe } from './pipes/search.pipe';
import { MyModuleModule } from 'src/app/my-module/my-module.module';
import { MyLazyModule } from 'src/app/my-lazy/my-lazy.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthService } from 'src/app/services/auth.service';
import { TopSecretComponent } from './top-secret/top-secret.component';
import { MapToIterablePipe } from './pipes/map-to-iterable.pipe';
import { ErrorsComponent } from './components/errors/errors.component';
import { TranslatePipe } from './pipes/translate.pipe';
import { ItemDetailsComponent } from './components/item-details/item-details.component';

const lazy = './my-lazy/my-lazy.module#MyLazyModule';

@NgModule({
  declarations: [
    AppComponent,
    ItemsComponent,
    WorkersComponent,
    RegisterFormComponent,
    SearchComponent,
    DatagridComponent,
    AuthComponent,
    AddItemComponent,
    SearchPipe,
    NotFoundComponent,
    TopSecretComponent,
    MapToIterablePipe,
    ErrorsComponent,
    TranslatePipe,
    ItemDetailsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: 'items', component: ItemsComponent },
      { path: 'workers', component: WorkersComponent },
      { path: 'registerForm', component: RegisterFormComponent },
      { path: 'secret', component: TopSecretComponent, canActivate: [AuthService] },
      { path: 'lazy', loadChildren: lazy },
      { path: 'item-details/:id', component: ItemDetailsComponent },
      { path: '**', component: NotFoundComponent}
    ]),
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot(),
    MyModuleModule,
    MyLazyModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: CORSInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
