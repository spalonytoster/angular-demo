import { browser, by, element, ElementFinder } from 'protractor';

export class AppPage {

  uniqueTitle: string;
  addItemForm: ElementFinder;
  searchByTitle: ElementFinder;

  constructor() {
    this.addItemForm = element(by.id('add-item-form'));
    this.uniqueTitle = `${Date.now()}`;
    this.searchByTitle = element(by.id('search-by-title'));
  }

  navigateTo() {
    return browser.get('/items');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  getLoginButton() {
    return element(by.buttonText('log in'));
  }

  getAddItemButton() {
    return element(by.buttonText('Add'));
  }

  fillAndSubmitAddItemForm(): any {
    this.addItemForm.element(by.name('title')).sendKeys(this.uniqueTitle);
    this.addItemForm.element(by.name('price')).sendKeys(12);
    this.addItemForm.element(by.name('category'))
      .element(by.cssContainingText('option', 'food'))
        .click();
    this.addItemForm.element(by.buttonText('Add item')).click();
  }

  findItem(): any {
    this.searchByTitle.sendKeys(this.uniqueTitle);
    return this.searchByTitle.getAttribute('value');
  }

  getItems(): any {
    element(by.tagName('ca-datagrid')).all(by.css('tbody tr'));
  }
}
