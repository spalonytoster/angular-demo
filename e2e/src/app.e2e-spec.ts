import { AppPage } from './app.po';
import { browser } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeAll(() => {
    page = new AppPage();
    page.navigateTo();
    expect(page.getLoginButton().isPresent()).toBeTruthy();
    page.getLoginButton().click();
    expect(page.getLoginButton().isPresent()).toBeFalsy();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to ngAppGdansk!');
  });

  it('should display add item form', () => {
    page.navigateTo();
    page.getAddItemButton().click();
    // expect(page.getAddItemButton().isPresent()).toBeTruthy();
    page.fillAndSubmitAddItemForm();
    // expect(page.getAddItemButton().isPresent()).toBeFalsy();
    browser.sleep(3000);
    expect(page.findItem()).toBe(page.uniqueTitle);
  });
});
